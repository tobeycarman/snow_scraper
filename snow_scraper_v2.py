#!/usr/bin/env python

# Script to plot JH snow data
# Patrick Wright, Tobey Carman
# Dec. 2014

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas.tseries.offsets import *
import datetime as dt
import sys

TEMP_SNOW_STNS = ['raymer','mid','rendezvous']
WIND_STNS = ['raymer', 'summit']
TIME_RES = ['1h', '15min']


def usage():
    print """Usage:
    python snow_scraper_v2.py <argv1> <argv2> <argv3> 
    <argv1> is snow and air temp data, can be "raymer", "mid", or "rendezvous"
    <argv2> is wind data, can be "raymer" or "summit"
    <argv3> is time resolution, can be "1h" for 1 hour, or "15min" for 15 min"""

def bail():
  usage()
  sys.exit(-1)

# Check number of args
if len(sys.argv) < 4:
  bail()

# Check valid args...
if sys.argv[1] not in TEMP_SNOW_STNS:
  bail()
if sys.argv[2] not in WIND_STNS:
  bail()
if sys.argv[3] not in TIME_RES:
  bail()

# OK, assign arguments to local variables
snow_site = sys.argv[1]
wind_site = sys.argv[2]
time_res = sys.argv[3]

print 'plotting data for...',snow_site, wind_site, time_res

#-----------------------------------------------------------------------------------------
# SETUP PARAMETERS FOR FILE IMPORT
#-----------------------------------------------------------------------------------------

# SNOW SITES, 1 HR
if snow_site == 'raymer' and time_res == '1h':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RAYMER.txt'
  title = "Raymer Study Plot 9,360' (hourly)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  skiprows_snow = 6
if snow_site == 'mid' and time_res == '1h':
  url_snow = 'http://wxstns.net/wxstns/jhnet/MID.txt'
  title = "Mid Mountain Study Plot 8,180' (hourly)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  skiprows_snow = 6
if snow_site == 'rendezvous' and time_res == '1h':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RVBOWL.txt'
  title = "Rendezvous Bowl Plot 9,580' (hourly)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  skiprows_snow = 7
  
# SNOW SITES, 15 MIN
if snow_site == 'raymer' and time_res == '15min':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RAYMER2.txt'
  title = "Raymer Study Plot 9,360' (15 min)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  skiprows_snow = 6
if snow_site == 'mid' and time_res == '15min':
  url_snow = 'http://wxstns.net/wxstns/jhnet/MID2.txt'
  title = "Mid Mountain Study Plot 8,180' (15 min)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  skiprows_snow = 6
if snow_site == 'rendezvous' and time_res == '15min':
  url_snow = 'http://wxstns.net/wxstns/jhnet/RVBOWL2.txt'
  title = "Rendezvous Bowl Plot 9,580' (15 min)"
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']
  skiprows_snow = 7
  
# WIND SITES, 1 HR
if wind_site == 'raymer' and time_res == '1h':
  url_wind = 'http://wxstns.net/wxstns/jhnet/RAYMRWND.txt'
  name_list_wind=['Date','Time','NaN1','Battery','NaN2','NaN3','Wind_Speed',
  'NaN4','Wind_Dir', 'NaN5','NaN6','Max_Gust']
  wind_title = "Wind at Raymer, 9,360'"
  skiprows_wind = 6
if wind_site == 'summit' and time_res == '1h':
  url_wind = 'http://wxstns.net/wxstns/jhnet/SUMMIT.txt'
  name_list_wind=['Date','Time','NaN1','NaN2','Summit_Air_Temp','NaN3','NaN4','Wind_Speed',
  'NaN5', 'Wind_Dir', 'NaN6', 'NaN7', 'Max_Gust'] 
  wind_title = "Wind at Summit, 10,450'"
  skiprows_wind = 7
  
# WIND SITES, 15 MIN 
if wind_site == 'raymer' and time_res == '15min':
  url_wind = 'http://wxstns.net/wxstns/jhnet/RAYMRWND2.txt'
  wind_title = "Wind at Raymer, 9,360'"
  name_list_wind=['Date','Time','NaN1','Battery','NaN2','NaN3','Wind_Speed',
  'NaN4','Wind_Dir', 'NaN5','NaN6','Max_Gust']
  skiprows_wind = 6
if wind_site == 'summit' and time_res == '15min':
  url_wind = 'http://wxstns.net/wxstns/jhnet/SUMMIT2.txt'
  wind_title = "Wind at Summit, 10,450'"
  name_list_wind=['Date','Time','NaN1','NaN2','Summit_Air_Temp','NaN3','NaN4','Wind_Speed',
  'NaN5', 'Wind_Dir', 'NaN6', 'NaN7', 'Max_Gust'] 
  skiprows_wind = 7
  
#-----------------------------------------------------------------------------------------

def main():
  
  #----------------------------------------------------------------------------
  # FORMAT FOR SNOW DATA
  #----------------------------------------------------------------------------

  # Read in data:
  data = pd.io.parsers.read_csv(url_snow, skiprows=skiprows_snow, sep='  ', engine='python', 
  skipfooter=1, index_col=False, header=None, names=name_list)
  #print data
  
  # Get a datestring that the 'infer_datetime_format' function can recognize
  datestring = data['Date'].apply(str) + data['Time'].apply(str)

  # Convert string to a Pandas datetime object
  dt = pd.to_datetime(datestring, infer_datetime_format=True)

  # set index of dataframe to datetime
  data_date = data.set_index(dt)
  
  # routine to get last 24 new snow and water content totals:
  current = data_date.index[0]
  minus24 = current - np.timedelta64(24,'h')
  subset = data_date[current:minus24]
  newlast24 = subset.Int_snow.max()
  
  if time_res == '1h':
    newlast24_index = subset.Int_snow.idxmax() + pd.DateOffset(hours=1)
  if time_res == '15min':
    newlast24_index = subset.Int_snow.idxmax() + pd.DateOffset(minutes=15)
  
  waterlast24_range = data_date.Precip[newlast24_index:(newlast24_index - np.timedelta64(24,'h'))]
  waterlast24 = waterlast24_range.sum() 

  data_date.sort_index(inplace=True) #reverse order of data to plot most recent on right
  #print data_date

  #----------------------------------------------------------------------------
  # FORMAT FOR WIND DATA 
  #----------------------------------------------------------------------------

  # Read in data:
  data_wind = pd.io.parsers.read_csv(url_wind, skiprows=skiprows_wind, sep='  ', engine='python', 
  skipfooter=1, index_col=False, header=None, names=name_list_wind)
  #print data_wind

  #Get a datestring that the 'infer_datetime_format' function can recognize
  datestring = data_wind['Date'].apply(str) + data_wind['Time'].apply(str)

  #Convert string to a Pandas datetime object
  dt_wind = pd.to_datetime(datestring, infer_datetime_format=True)

  #set index of dataframe to datetime
  data_wind_date = data_wind.set_index(dt_wind)
  data_wind_date.sort_index(inplace=True)
  #print data_wind_date

  #----------------------------------------------------------------------------
  # PLOTTING...
  #----------------------------------------------------------------------------

  # look at length of data_date and data_wind_date, trim to length of the shorter dataset:
  print data_date.index.min(), data_wind_date.index.min()
  if data_date.index.min() < data_wind_date.index.min():
    print "The data record goes farther back than the wind data record. Trimming the data...."
    data_date_trim = data_date[data_wind_date.index.min():data_wind_date.index.max()]
    data_wind_date_trim = data_wind_date # wind index stays unchanged
  if data_date.index.min() > data_wind_date.index.min():
    print "The wind data record goes farther back than the data record. Trimming the wind data...."
    data_wind_date_trim = data_wind_date[data_date.index.min():data_date.index.max()]
    data_date_trim = data_date # snow index stays unchanged
  else:
    print "The datas have the same beginning! Useing them all..."
    data_date_trim = data_date
    data_wind_date_trim = data_wind_date


  # plot window set to 17" x 8"
  fig = plt.figure(figsize=(17,8))

  # generate series for plotting x-axis for snow and temp data
  x = data_date_trim.index.to_pydatetime()    

  # AIR TEMP
  ax1 = fig.add_subplot(211)
  ax1.plot(x,data_date_trim.Air_Temp, color='red')
  ax1.set_ylabel('Air Temp (F)',color='r')
  ax1.set_ylim([-20,40])
  ax1.set_title(title, fontsize=14)
  ax1.axhline(y=32.0, linewidth=0.5, color='r', zorder=0, ls='--')
  #ax1.xaxis.set_ticklabels([])
  ax1.xaxis.grid(True, which='major')
  for tl in ax1.get_yticklabels():
      tl.set_color('r')
      
  # SNOW DEPTH
  ax12 = ax1.twinx()
  ax12.plot(x,data_date_trim.Ttl_Snow, color='black')
  ax12.set_ylabel('Snow Depth (in)', color='black')
  #ax12.set_ylim([(min(data_date.Ttl_Snow)-5),(max(data_date.Ttl_Snow) + 10)])
  ax12.set_ylim([40,70])
  for tl in ax12.get_yticklabels():
      tl.set_color('black')
      
  # NEW SNOW
  ax13 = ax1.twinx()
  ax13.spines['right'].set_position(('axes',1.05))
  ax13.set_frame_on(True)
  ax13.patch.set_visible(False)
  ax13.plot(x,data_date_trim.Int_snow, color='b', label='new snow')
  ax13.set_ylabel('New Snow (in)', color='b')
  plt.ylim([0,15])
  for tl in ax13.get_yticklabels():
      tl.set_color('b')

  # WATER CONTENT
  ax14 = ax1.twinx()
  ax14.spines['right'].set_position(('axes',1.1))
  ax14.set_frame_on(True)
  ax14.patch.set_visible(False)
  ax14.plot(x,data_date_trim.Precip, color='b', ls='--', label='water content')
  ax14.set_ylabel('Water Content (in)', color='b')
  plt.ylim([0,0.3])
  for tl in ax14.get_yticklabels():
      tl.set_color('b')
  h1, l1 = ax13.get_legend_handles_labels()
  h2, l2 = ax14.get_legend_handles_labels()
  ax14.legend(h1+h2, l1+l2, loc='upper left', bbox_to_anchor=(0.01,1.19),prop={'size':10})
  
  # make a text box for new snow and water content 24 hr totals:
  left, width = .48, .5
  bottom, height = .25, .8
  right = left + width
  top = bottom + height
  ax14.text(right, top, ('%s" new snow, %s" water content\n(24 hr totals as of %s)'
    %(newlast24,waterlast24,newlast24_index)),
        horizontalalignment='right',
        verticalalignment='bottom',
        style='italic',
        transform=ax14.transAxes,
        bbox=dict(facecolor='white', edgecolor='black', pad=10.0))
  
 #----------------------------------------------------------------------------  
 
  # generate series for plotting x-axis for wind data
  x_wind = data_wind_date_trim.index.to_pydatetime()

  # WIND DIRECTION
  ax2 = fig.add_subplot(212)
  ax2.plot(x_wind,data_wind_date_trim.Wind_Dir, color='black')
  ax2.set_ylabel('Wind Direction', color='black')
  ax2.set_ylim([0,360])
  ax2.axhline(y=270.0, linewidth=0.5, color='black', zorder=0, ls='--')
  ax2.axhline(y=180.0, linewidth=0.5, color='black', zorder=0, ls='--')
  ax2.xaxis.grid(True, which='major')
  for tl in ax2.get_yticklabels():
      tl.set_color('black')

  # MAX GUST    
  ax22 = ax2.twinx()
  ax22.plot(x_wind,data_wind_date_trim.Max_Gust, color='blue', ls='--', label='max gust')
  #ax22.set_ylabel('Max Gust (mph)', color='blue')
  ax22.set_ylim([0,70])
  ax22.axes.get_yaxis().set_visible(False)
  #ax22.spines['right'].set_position(('axes',1.07))
  #for tl in ax22.get_yticklabels():
      #tl.set_color('blue')

  # WIND SPEED
  ax23 = ax2.twinx()
  ax23.plot(x_wind, data_wind_date_trim.Wind_Speed, label='wind speed')
  ax23.set_ylabel('Wind Speed (mph)', color='b')
  ax23.set_ylim([0,70])
  ax23.set_title(wind_title, fontsize=12)
  for tl in ax23.get_yticklabels():
      tl.set_color('b')
  h1, l1 = ax22.get_legend_handles_labels()
  h2, l2 = ax23.get_legend_handles_labels()
  ax23.legend(h1+h2, l1+l2, loc=3,prop={'size':10})

  #fig.subplots_adjust(bottom=0.3)
  fig.subplots_adjust(right=.87)
  fig.subplots_adjust(left=.06)        
  #fig.subplots_adjust(top=0.8)

  #from IPython import embed
  #embed()
  #plt.ion()
  #plt.draw() # execute this if updates are not being drawn on plot

  print 'Done.'
  plt.show()

if __name__ == '__main__': 

  main()
