#!/usr/bin/env python

# Script to plot JH snow data
# Patrick Wright
# University of Montana
# Dec. 2014

# may be necessary to install lxml, html5lib, for Pandas' initial parsing attempts

#sys.path.append("/usr/local/lib/python2.7/dist-packages")
#import nltk 

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys

print "Let it snow!"

site = sys.argv[1]

if site == 'raymer':
  url = 'http://wxstns.net/wxstns/jhnet/RAYMER.txt'
  title = "Raymer Study Plot 9,360'"
  wind = 'http://wxstns.net/wxstns/jhnet/RAYMRWND.txt'
if site == 'mid':
  url = 'http://wxstns.net/wxstns/jhnet/MID.txt'
  title = "Mid Mountain Study Plot 8,180' (hourly)"
  wind = 'http://wxstns.net/wxstns/jhnet/RAYMRWND.txt' 

# SOUP TECHNIQUES:
#url = "http://wxstns.net/wxstns/jhnet/RAYMER.txt"    
#html = urlopen(url).read()
#RAYMER = soup(html)
#data = RAYMER.get_text()
##print data

# Main program:
def main():
  #----------------------------------------------------------------------------
  # FORMAT FOR RAYMER & MID SNOW DATA - hourly
  #----------------------------------------------------------------------------
  name_list=['Date','Time','NaN1','Battery','NaN2','NaN3','Air_Temp','NaN4','NaN5',
  'RH','NaN6','NaN7','Int_snow','NaN8','NaN9','Ttl_Snow','NaN9','Precip']

  # Read in data:
  data = pd.io.parsers.read_csv(url, skiprows = 6, sep='  ', engine='python', nrows=153, 
  index_col=False, header=None, names=name_list)

  # If parsing with read_csv, this works:
  #parse_dates={'datetime':[0,1]}

  # Get a datestring that the 'infer_datetime_format' function can recognize
  datestring = data['Date'].apply(str) + data['Time'].apply(str)

  # Convert string to a Pandas datetime object
  dt = pd.to_datetime(datestring, infer_datetime_format=True)

  # set index of dataframe to datetime
  data_date = data.set_index(dt)
  data_date.sort_index(inplace=True)#(axis=0,ascending=True,inplace=True)
  #print data_date

  #----------------------------------------------------------------------------
  # FORMAT FOR RAYMER WIND DATA - hourly
  #----------------------------------------------------------------------------
  name_list_wind=['Date','Time','NaN1','Battery','NaN2','NaN3','Wind_Speed','NaN4','Wind_Dir', 'NaN5','NaN6','Max_Gust']

  # Read in data:
  data_wind = pd.io.parsers.read_csv(wind, skiprows = 6, sep='  ', engine='python', nrows=154, 
  index_col=False, header=None, names=name_list_wind)
  #print data_wind

  # If parsing with read_csv, this works:
  #parse_dates={'datetime':[0,1]}

  #Get a datestring that the 'infer_datetime_format' function can recognize
  datestring = data_wind['Date'].apply(str) + data_wind['Time'].apply(str)

  #Convert string to a Pandas datetime object
  dt_wind = pd.to_datetime(datestring, infer_datetime_format=True)

  #set index of dataframe to datetime
  data_wind_date = data_wind.set_index(dt_wind)
  data_wind_date.sort_index(inplace=True)#(axis=0,ascending=True,inplace=True)
  #print data_wind_date

  #----------------------------------------------------------------------------
  # PLOTTING...
  #----------------------------------------------------------------------------

  # plot window set to 17" x 8"
  fig = plt.figure(figsize=(17,8))

  # generate series for plotting x-axis for snow data
  x = data_date.index.to_pydatetime()    

  # AIR TEMP
  ax1 = fig.add_subplot(211)
  ax1.plot(x,data_date.Air_Temp, color='red')
  ax1.set_ylabel('Air Temp (F)',color='r')
  ax1.set_ylim([-20,40])
  ax1.set_title(title, fontsize=14)
  ax1.axhline(y=32.0, linewidth=0.5, color='r', zorder=0, ls='--')
  ax1.xaxis.set_ticklabels([])
  ax1.xaxis.grid(True, which='major')
  for tl in ax1.get_yticklabels():
      tl.set_color('r')
      
  # SNOW DEPTH
  ax12 = ax1.twinx()
  ax12.plot(x,data_date.Ttl_Snow, color='black')
  ax12.set_ylabel('Snow Depth (in)', color='black')
  ax12.set_ylim([(min(data_date.Ttl_Snow)-5),(max(data_date.Ttl_Snow) + 10)])
  for tl in ax12.get_yticklabels():
      tl.set_color('black')
      
  # NEW SNOW
  ax13 = ax1.twinx()
  ax13.spines['right'].set_position(('axes',1.07))
  ax13.set_frame_on(True)
  ax13.patch.set_visible(False)
  ax13.plot(x,data_date.Int_snow, color='b')
  ax13.set_ylabel('New Snow (in)', color='b')
  plt.ylim([0,15])
  for tl in ax13.get_yticklabels():
      tl.set_color('b')

  # WATER CONTENT
  ax14 = ax1.twinx()
  ax14.spines['right'].set_position(('axes',1.11))
  ax14.set_frame_on(True)
  ax14.patch.set_visible(False)
  ax14.plot(x,data_date.Precip, color='b', ls='--')
  ax14.set_ylabel('Water Content (in)', color='b')
  plt.ylim([0,0.75])
  for tl in ax14.get_yticklabels():
      tl.set_color('b')

  # generate series for plotting x-axis for wind data
  x_wind = data_wind_date.index.to_pydatetime()

  # RAYMER WIND DIRECTION
  ax2 = fig.add_subplot(212)
  ax2.plot(x_wind,data_wind_date.Wind_Dir, color='black')
  ax2.set_ylabel('Wind Direction', color='black')
  ax2.set_ylim([0,360])
  ax2.axhline(y=270.0, linewidth=0.5, color='black', zorder=0, ls='--')
  ax2.axhline(y=180.0, linewidth=0.5, color='black', zorder=0, ls='--')
  ax2.xaxis.grid(True, which='major')
  for tl in ax2.get_yticklabels():
      tl.set_color('black')

  # RAYMER MAX GUST    
  ax22 = ax2.twinx()
  ax22.plot(x_wind,data_wind_date.Max_Gust, color='blue', ls='--', label='max gust')
  ax22.set_ylabel('Max Gust (mph)', color='blue')
  ax22.set_ylim([0,60])
  ax22.spines['right'].set_position(('axes',1.07))
  for tl in ax22.get_yticklabels():
      tl.set_color('blue')

  # RAYMER WIND SPEED
  ax23 = ax2.twinx()
  ax23.plot(x_wind, data_wind_date.Wind_Speed, label='wind speed')
  ax23.set_ylabel('Wind Speed (mph)', color='b')
  ax23.set_ylim([0,40])
  ax23.set_title("Wind at Raymer 9,360' (hourly)", fontsize=12)
  for tl in ax23.get_yticklabels():
      tl.set_color('b')
  h1, l1 = ax22.get_legend_handles_labels()
  h2, l2 = ax23.get_legend_handles_labels()
  ax23.legend(h1+h2, l1+l2, loc=4,prop={'size':10})

  #fig.subplots_adjust(bottom=0.3)
  fig.subplots_adjust(right=.87)
  fig.subplots_adjust(left=.093)        
  #fig.subplots_adjust(top=0.8)

  #from IPython import embed
  #embed()
  #plt.ion()
  #plt.draw() # execute this if updates are not being drawn on plot

  plt.show()
  
if __name__ == '__main__': 

  main()
